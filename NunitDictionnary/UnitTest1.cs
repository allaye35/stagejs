using WebApplication3.Models;

namespace NunitDictionnary;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test1()
    {
        
       Dictionary<object, List<object>> dico1 = new Dictionary<object, List<object>>();
        Dictionary<object, List<object>> dico2 = new Dictionary<object, List<object>>();
        
        Dictionary<object, List<object>> computedDico = new Dictionary<object, List<object>>();

        List<object> values1 = new List<object>();
        List<object> values2 = new List<object>();
        List<object> values3 = new List<object>() ;
        List<object> values4 = new List<object>();
        List<object> values5 = new List<object>();
        List<object> values6 = new List<object>();
        
        object k1 = "key1";
        object k2 = "key2";
        object k3 = "key3";
        object k4 = "key4";
        
        object v1 = "value1";
        object v2 = "value2";
        object v3 = "value3";
        
        object v4 = "value4";
        object v5 = "value5";
        object v6 = "value6";

        object v7 = "value7";
        object v8 = "value8";
        object v9 = "value9";

        values1.Add(v1);
        values1.Add(v2);
        values2.Add(v2);
        values2.Add(v3);
        
        values3.Add(v4);
        values3.Add(v5);
        values4.Add(v5);
        values4.Add(v6);

        values5.Add(v7);
        values5.Add(v8);
        values6.Add(v8);
        values6.Add(v9);
        
        dico1.Add(k1,values1);
        
        dico1.Add(k2,values3);
        dico2.Add(k2,values4);
        
        dico2.Add(k3,values6);

        dico1.Add(k4,values2);
        dico2.Add(k4,values2);

        computedDico.Add(k1,values1); // Cas clé dans un seul des deux dico
        computedDico.Add(k2,new List<object>() { {v4} , {v6}} ); // Cas clé dans les deux dicos, seul les valeurs pas communes sont retournées
        computedDico.Add(k3,values6);  // Cas clé dans un seul des deux dico
        
        Dictionary<object, List<object>> utilDico = CompareDictionary.DictionariesEqual(dico1, dico2);
        
        Assert.That(computedDico, Is.EqualTo(utilDico));
        
    }
    
    
  

       

        [TestCase]
        public void DictionariesEqual_CompareEmptyDictionaries_ReturnsEmptyDictionary()
        {
            // Arrange
            var dico1 = new Dictionary<object, List<object>>();
            var dico2 = new Dictionary<object, List<object>>();

            // Act
            var result = CompareDictionary.DictionariesEqual(dico1, dico2);

            // Assert
            Assert.That(result, Is.Empty);
        }

        [TestCase]
        public void DictionariesEqual_CompareIdenticalDictionaries_ReturnsEmptyDictionary()
        {
            // Arrange
            var dico1 = new Dictionary<object, List<object>>
            {
                { "key1", new List<object> { "value1", "value2" } },
                { "key2", new List<object> { "value4", "value5" } }
            };

            var dico2 = new Dictionary<object, List<object>>
            {
                { "key1", new List<object> { "value1", "value2" } },
                { "key2", new List<object> { "value4", "value5" } }
            };

            // Act
            var result = CompareDictionary.DictionariesEqual(dico1, dico2);

            // Assert
            Assert.That(result, Is.Empty);
        }

        

    

       
        

            [TestCase]
            public void DictionariesEqual_CompareDifferentDictionariesWithNoCommonKeys_ReturnsCombinedDictionary()
            {
                // Arrange
                var dico1 = new Dictionary<object, List<object>>
                {
                    { "key1", new List<object> { "value1", "value2" } },
                    { "key2", new List<object> { "value4", "value5" } }
                };

                var dico2 = new Dictionary<object, List<object>>
                {
                    { "key3", new List<object> { "value6", "value7" } },
                    { "key4", new List<object> { "value8", "value9" } }
                };

                var expectedDico = new Dictionary<object, List<object>>
                {
                    { "key1", new List<object> { "value1", "value2" } },
                    { "key2", new List<object> { "value4", "value5" } },
                    { "key3", new List<object> { "value6", "value7" } },
                    { "key4", new List<object> { "value8", "value9" } }
                };

                // Act
                var result = CompareDictionary.DictionariesEqual(dico1, dico2);

                // Assert
                Assert.That(result, Is.EqualTo(expectedDico));
            }
            
        }




            
        




    


    
    