﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication3.Models;
using WebApplication3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication3.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication3.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication3.Models;
using System.Collections.Generic;

namespace WebApplication3.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
    
    public IActionResult CompareDictionarys()
    {
        
        

        return View("CompareDictionary");
    }
    
    public IActionResult HelloWord()
    {
        return View("HelloWord");
    }
    
    public IActionResult Hello()
    {
        
        return View("Hello");
    }
    public IActionResult CompareDictionary(Dictionary<string, int> dict1, Dictionary<string, int> dict2)
    {


        return View("CompareDictionary");
    }

    public IActionResult CustomHtmlHelper()
    {


         var    AvailableOptions = new List<SelectListItem>
        {
            new SelectListItem { Value = "option1", Text = "Option 1" },
            new SelectListItem { Value = "option2", Text = "Option 2" },
            new SelectListItem { Value = "option3", Text = "Option 3" },
            // Ajoutez d'autres options ici

        };
        /*CustomHtmlHelpers customHtmlHelpers = new _Page_Views_Home_CustomHtmlHelpers_cshtml();*/
     
        return View("CustomHtmlHelpers");
    }
    public ActionResult NVelocityTag()
    {
        var model = new PersonModel { Name = "Drogba", Age = 20 };

        // Utilisation de NVelocityTag pour évaluer le template et récupérer le contenu généré
        NVelocityTag nVelocityTag = new NVelocityTag();
        string generatedContent = nVelocityTag.RenderTemplate("./Views/Home/NVelocityTag.vm", model.Name);

        return Content(generatedContent, "text/html");
    }
    public IActionResult VelocityController()
    {
        var model = new PersonModel { Name = "Kyan", Age = 10 };

        VelocityTag velocityTag = new VelocityTag();
        string templateFile = "./Views/Home/VelocityTag.vm";

        string generatedContent = velocityTag.RenderTemplate(templateFile, model.Name, model.Age);

        return Content(generatedContent, "text/html");
    }
}