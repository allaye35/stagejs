using Commons.Collections;
using NVelocity.App;
using System.IO;
using NVelocity;
using NVelocity.Context;
using NVelocity.Runtime;
using Commons.Collections;
using NVelocity.App;

using System.IO;
using NVelocity;
using NVelocity.Context;
using NVelocity.Runtime;
using System.IO;
using NVelocity;
using NVelocity.Context;
using NVelocity.Runtime;
namespace WebApplication3.Models;

public class VelocityTag
{
    public string RenderTemplate(string templateName, string name, int age)
        {
            VelocityEngine engine = new VelocityEngine();
            engine.Init();
    
            VelocityContext context = new VelocityContext();
            context.Put("name", name);
            context.Put("age", age);
    
            Template template = engine.GetTemplate(templateName);
            StringWriter writer = new StringWriter();
            template.Merge(context, writer);
    
            return writer.ToString();
        }
    
}