namespace WebApplication3.Models;

public class PersonModel
{
    public string Name { get; set; }
    public int Age { get; set; }
}