using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace WebApplication3.Models;


public static class CustomHtmlHelpers
{
    public static IHtmlContent MultiSelectWithChoice(
        this IHtmlHelper htmlHelper,
        string name,
        IEnumerable<SelectListItem> selectList,
        object htmlAttributes = null)
    {
        var id = htmlHelper.IdForModel();

        var selectTag = new TagBuilder("select");
        selectTag.MergeAttribute("id", id);
        selectTag.MergeAttribute("name", name);
        selectTag.MergeAttribute("multiple", "multiple");
        selectTag.MergeAttributes(new RouteValueDictionary(htmlAttributes));

        var options = new StringBuilder();

        foreach (var item in selectList)
        {
            var optionTag = new TagBuilder("option");
            optionTag.MergeAttribute("value", item.Value);
            optionTag.InnerHtml.AppendHtml(item.Text);
            options.Append(optionTag.ToString());
        }

        selectTag.InnerHtml.AppendHtml(options.ToString());

        // Add Choice.js script to the page
        var script = new TagBuilder("script");
        script.InnerHtml.AppendHtml($@"
            document.addEventListener('DOMContentLoaded', function() {{
                new Choices('#{id}', {{ removeItemButton: true }});
            }});
        ");

        return new HtmlContentBuilder().AppendHtml(selectTag).AppendHtml(script);
    }
}