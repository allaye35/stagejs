namespace WebApplication3.Models;

public class CompareDictionary
{
    public static Dictionary<object, List<object>> DictionariesEqual(Dictionary<object, List<object>> dico1, Dictionary<object, List<object>> dico2)
    {
        Dictionary<object, List<object>> result = new Dictionary<object, List<object>>();

        // Compare keys in dico1 with dico2
        foreach (var kvp in dico1)
        {
            object key = kvp.Key;
            List<object> values = kvp.Value;

            if (dico2.ContainsKey(key))
            {
                List<object> commonValues = GetCommonValues(values, dico2[key]);
                if (commonValues.Count > 0)
                    result.Add(key, commonValues);
            }
            else
            {
                result.Add(key, values);
            }
        }

        // Find keys in dico2 that are not in dico1
        foreach (var kvp in dico2)
        {
            object key = kvp.Key;
            List<object> values = kvp.Value;

            if (!dico1.ContainsKey(key))
                result.Add(key, values);
        }

        return result;
    }

    private static List<object> GetCommonValues(List<object> list1, List<object> list2)
    {
        List<object> commonValues = new List<object>();
        foreach (object value in list1)
        {
            if (!list2.Contains(value))
                commonValues.Add(value);
        }

        foreach (object value in list2)
        {
            if (!list1.Contains(value) && !commonValues.Contains(value))
                commonValues.Add(value);
        }

        return commonValues;
    }
}