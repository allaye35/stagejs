namespace WebApplication3.Models;
using Microsoft.AspNetCore.Html;

public class HelloWorld
{
    public static string Name { get; set; }

    public static HtmlString Hello()
    {
        Name = "HelloWorld";
        string html = $"<p>{Name}</p>";
        return new HtmlString(html);
    }
}