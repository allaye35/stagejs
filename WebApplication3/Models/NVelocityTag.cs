using Commons.Collections;
using NVelocity.App;
using System.IO;
using NVelocity;
using NVelocity.Context;
using NVelocity.Runtime;

namespace WebApplication3.Models;

public class NVelocityTag
{
    public string RenderTemplate(string templateFile, string name)
    {
        /*
        // Configuration de NVelocity
        VelocityEngine velocity = new VelocityEngine();
        ExtendedProperties props = new ExtendedProperties();
        props.AddProperty(RuntimeConstants.RESOURCE_LOADER, "file");
        props.AddProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, templatePath); // Chemin vers le template
        velocity.Init(props);

        // Évaluer le template NVelocity avec le modèle
        StringWriter writer = new StringWriter();
        velocity.Evaluate(new VelocityContext(), writer, null, "toto");//model.ToString());

        // Récupérer le contenu généré par NVelocity en tant que chaîne de caractères
        string result = writer.ToString();
        */

        
        
        VelocityEngine engine = new VelocityEngine();
        engine.Init();

        VelocityContext context = new VelocityContext();
        context.Put("name", name);
        
        Template template = engine.GetTemplate(templateFile);
        StringWriter writer = new StringWriter();
        template.Merge(context, writer);


        return writer.ToString();
    }
}