using Microsoft.AspNetCore.Html;

namespace WebApplication3.Models;

public class Hello
{
    public static string Name { get; set; }

    public static HtmlString SayHello(string Nom)
    {
        //Name = "HelloWorld";
        //string html = $"<p>{Name} {Nom}</p>";
        
        string html = $"{Nom}";
        return new HtmlString(html);
    }
    
}